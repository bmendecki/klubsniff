var app = angular.module('klubSniff',['ngSanitize']);

app.controller('InputController',function($scope,$http){
    $scope.userInput = '';    
    $scope.response = [];
    $scope.phrase = "";
    $scope.isSearch = false;
    
    $scope.kryteria ={
            a  : false,
            b  : false,
            c  : false,
            d  : false,
            e  : false
        }; 
    
    $scope.getUrls = function() {
        $scope.isSearch = true;       
        $scope.listOfUrls = $scope.userInput.split('\n');
        $('#wait').fadeIn();
        
               
        var params = JSON.stringify($scope.kryteria);
        //console.log($scope.kryteria);
       
        
        var length = $scope.listOfUrls.length;
        var promises = [];

            
            for(let i=0;i<length;i++) {
            
                let url = $scope.listOfUrls[i];            
                
                   var promise = $http.get('http.php',{
                            params: {
                                "url" : url,
                                "phrase" : $scope.phrase,
                                "params" : params
                                
                            }}).then(function(response) {
                            
                            //console.log(response);
                            ($scope.response).push(response);                        
                        });
                promises.push(promise);
                }
                
               Promise.all(promises).then(function() {
                //console.log('skonczone' + promises.length);
               
                    $('#wait').fadeOut();
                    $('#btn').hide();
                    $('#reset').show();
                    
                    $('#main').DataTable( {
                        dom: 'Bfrtip',
                        buttons: [ 'copy', 'csv', 'excel', 'pdf', 'print']                    
                    });                
                
               });               
    };
    
   $('[data-toggle="tooltip"]').tooltip();
       $('#reset').on('click', function(){
         var url = $(location).attr('href');     
         window.location.replace(url);
         });
});


