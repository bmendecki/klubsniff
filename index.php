<?
include_once('config.php');
//include_once(DOCUMENT_ROOT.'klubsniff/functions/search.php');

?>
<!DOCTYPE html>
<html ng-app="klubSniff">
   <head>
   <meta charset="utf-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">  
  <link rel="stylesheet" href="css/style.css">
  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.3/jquery.min.js"></script>
  
  <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
  <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
  <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
  
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
  <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-sanitize.js"></script>
 
 
  <script src="js/app.js"></script>
</head>
<body>

<h3>Szukamy śladów klubu rabatowego na stronach</h3>

<div ng-controller="InputController">
 
   <form name="mainform">
    <div class="form-group" ng-class="{ 'has-error' : userInput.$error.required}">
      <textarea placeholder="wstaw urle jeden pod drugin" ng-model="userInput" name="userInput" rows="15" cols="200" class="form-control" required></textarea>
    </div>    
      <div class="form-group">
      <a href="#" data-toggle="tooltip" title="czy jest fraza 'rabatow' na index.php">
       <label class="checkbox-inline"><input type="checkbox" name="q1" value="1" ng-model="kryteria.a"/> klub</label></a>
       <a href="#" data-toggle="tooltip" title="czy jest fraza 'rabatow' na order2.php">
       <label class="checkbox-inline"><input type="checkbox" name="q2" value="1" ng-model="kryteria.b"/>klub order2 </label>
       </a>
       <a href="#" data-toggle="tooltip" title="czy jest fraza 'discount_club' w sesji">
       <label class="checkbox-inline"><input type="checkbox" name="q3" value="1" ng-model="kryteria.c"/> discount_club w sesji </label></a>
       <a href="#" data-toggle="tooltip" title="czy jest fraza 'REGULAMIN KORZYSTANIA Z KUPONOWEGO KLUBU RABATOWEGO' w regulaminie">
       <label class="checkbox-inline"><input type="checkbox" name="q4" value="1" ng-model="kryteria.d"/> regulamin </label> </a>
       <a href="#" data-toggle="tooltip" title="czy jest fraza 'REGULAMIN KORZYSTANIA Z KUPONOWEGO KLUBU RABATOWEGO' w regulaminie">
       <label class="checkbox-inline"><input type="checkbox" name="q5" value="1" ng-model="kryteria.e"/> polityka </label></a>
       <span><img src="img/squares.gif" id="wait"/></span>
      </div>
      <div class="form-group">
      <label>wpisz dowolną frazę (case sensitive)<br>
      <input type="text" ng-model="phrase">
      </label>     
      </div>
       
   <button class="btn-lg" id="btn"
   ng-disabled="!kryteria.a && !kryteria.b && !kryteria.c && !kryteria.d && !kryteria.e || mainform.$invalid" ng-click="getUrls()">SZUKAJ</button>
   <span id="reset">Czyść wyniki</span>
   </form>
   
   
   <table class="table table-striped" id='main' ng-if="isSearch"  >
   <thead>
      <tr>
         <th>Adres strony</th>
         <th><a href="#" data-toggle="tooltip" title="artykuł czy sprzedażówka">LP/SP</a></th>
         <th ng-if="kryteria.a"><a href="#" data-toggle="tooltip" title="czy jest fraza 'rabatow' na index.php">klub</a></th>
         <th ng-if="kryteria.b"><a href="#" data-toggle="tooltip" title="czy jest fraza 'rabatow' na order2.php">klub ord2</a></th>
         <th ng-if="kryteria.c"></th>
         <th ng-if="kryteria.d"><a href="#" data-toggle="tooltip" title="czy jest fraza 'REGULAMIN KORZYSTANIA Z KUPONOWEGO KLUBU RABATOWEGO' w regulaminie">terms</a></th>
         <th ng-if="kryteria.e"><a href="#" data-toggle="tooltip" title="czy jest fraza 'Privacy Policy' w privacy.php">Privacy</a></th>
         <th ng-if="phrase"><a href="#">Fraza: {{phrase}}</a></th>
      </tr>
   </thead>
  
   <tr ng-bind-html="result.data" ng-repeat="result in response track by $index"> 
    
      
   </tr>      
   

   </table>

 
</div> <!-- eo main div -->
</body>
</html>